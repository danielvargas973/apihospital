﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace APIHospital.Model
{
    public class PatientModel
    {
        public PatientModel()
        {
        }
        public PatientModel(int id, string fullName, int socialSecurityName, int postCode, int telephoneContact)
        {
            Id = id;
            FullName = fullName;
            SocialSecurityName = socialSecurityName;
            PostCode = postCode;
            TelephoneContact = telephoneContact;
        }
        [JsonPropertyName("Id")]
        public int Id { get; set; }
        [JsonPropertyName("FullName")]
        public string FullName { get; set; }
        [JsonPropertyName("SocialSecurityName")]
        public int SocialSecurityName { get; set; }
        [JsonPropertyName("PostCode")]
        public int PostCode { get; set; }
        [JsonPropertyName("TelephoneContact")]
        public int TelephoneContact { get; set; }
    }
}
