﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace APIHospital.Model
{
    public class DoctorModel
    {
        public DoctorModel()
        {
        }
        public DoctorModel(int id, string fullName, string specialty, int credentialNumber, string nmeHospital)
        {
            Id = id;
            FullName = fullName;
            Specialty = specialty;
            CredentialNumber = credentialNumber;
            NmeHospital = nmeHospital;
        }
        [JsonPropertyName("Id")]
        public int Id { get; set; }
        [JsonPropertyName("FullName")]
        public string FullName { get; set; }
        [JsonPropertyName("Specialty")]
        public string Specialty { get; set; }
        [JsonPropertyName("CredentialNumber")]
        public int CredentialNumber { get; set; }
        [JsonPropertyName("NmeHospital")]
        public string NmeHospital { get; set; }
    }
}
