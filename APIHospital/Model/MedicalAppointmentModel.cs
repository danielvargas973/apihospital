﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace APIHospital.Model
{
    public class MedicalAppointmentModel
    {
        public MedicalAppointmentModel()
        {
        }
        public MedicalAppointmentModel(int id, int doctorId, int patientId, int officeNumber, string reasonQuote)
        {
            Id = id;
            DoctorId = doctorId;
            PatientId = patientId;
            OfficeNumber = officeNumber;
            ReasonQuote = reasonQuote;
        }
        [JsonPropertyName("Id")]
        public int Id { get; set; }
        [JsonPropertyName("DoctorId")]
        public int DoctorId { get; set; }
        [JsonPropertyName("PatientId")]
        public int PatientId { get; set; }
        [JsonPropertyName("OfficeNumber")]
        public int OfficeNumber { get; set; }
        [JsonPropertyName("ReasonQuote")]
        public string ReasonQuote { get; set; }
    }
}
