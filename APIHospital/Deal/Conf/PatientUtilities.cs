﻿using APIHospital.Entity;
using APIHospital.Model;
using System;
using System.Collections.Generic;

namespace APIHospital.Deal.Conf
{
    public class PatientUtilities
    {
        public static PatientModel TransformToModel(Patient Patient)
        {
            try
            {
                PatientModel PatientModel = new PatientModel(Patient.Id, Patient.ST_fullName, Patient.IN_socialSecurityName, Patient.IN_postCode,
                    Patient.IN_telephoneContact);
                return PatientModel;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }
        public static Patient TransformToEntity(PatientModel PatientModel)
        {
            try
            {
                Patient Patient = new Patient(PatientModel.Id, PatientModel.FullName, PatientModel.SocialSecurityName,
                    PatientModel.PostCode, PatientModel.TelephoneContact);
                return Patient;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }
        public static List<PatientModel> TransformToModelCollection(List<Patient> Patients)
        {
            try
            {
                List<PatientModel> PatientModels = new List<PatientModel>();
                foreach (Patient Patient in Patients)
                {
                    PatientModels.Add(TransformToModel(Patient));
                }
                return PatientModels;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }
    }
}
