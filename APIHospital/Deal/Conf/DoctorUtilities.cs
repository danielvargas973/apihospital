﻿using APIHospital.Entity;
using APIHospital.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIHospital.Deal.Conf
{
    public class DoctorUtilities
    {
        public static DoctorModel TransformToModel(Doctor Doctor)
        {
            try
            {
                DoctorModel doctorModel = new DoctorModel(Doctor.Id, Doctor.ST_fullName, Doctor.ST_specialty, Doctor.IN_credentialNumber,
                    Doctor.ST_nmeHospital);
                return doctorModel;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }
        public static Doctor TransformToEntity(DoctorModel doctorModel)
        {
            try
            {
                Doctor doctor = new Doctor(doctorModel.Id, doctorModel.FullName, doctorModel.Specialty,
                    doctorModel.CredentialNumber, doctorModel.NmeHospital);
                return doctor;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }
        public static List<DoctorModel> TransformToModelCollection(List<Doctor> doctors)
        {
            try
            {
                List<DoctorModel> doctorModels = new List<DoctorModel>();
                foreach (Doctor doctor in doctors)
                {
                    doctorModels.Add(TransformToModel(doctor));
                }
                return doctorModels;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }
    }
}
