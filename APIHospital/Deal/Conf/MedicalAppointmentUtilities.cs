﻿using APIHospital.Entity;
using APIHospital.Model;
using System;
using System.Collections.Generic;

namespace APIHospital.Deal.Conf
{
    public class MedicalAppointmentUtilities
    {
        public static MedicalAppointmentModel TransformToModel(MedicalAppointment MedicalAppointment)
        {
            try
            {
                MedicalAppointmentModel MedicalAppointmentModel = new MedicalAppointmentModel(MedicalAppointment.Id,
                    MedicalAppointment.DoctorId, MedicalAppointment.PatientId, MedicalAppointment.IN_officeNumber,
                    MedicalAppointment.ST_ReasonQuote);
                return MedicalAppointmentModel;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }
        public static MedicalAppointment TransformToEntity(MedicalAppointmentModel MedicalAppointmentModel)
        {
            try
            {
                MedicalAppointment MedicalAppointment = new MedicalAppointment(MedicalAppointmentModel.Id, 
                    MedicalAppointmentModel.DoctorId, MedicalAppointmentModel.PatientId, MedicalAppointmentModel.OfficeNumber,
                    MedicalAppointmentModel.ReasonQuote);
                return MedicalAppointment;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }
        public static List<MedicalAppointmentModel> TransformToModelCollection(List<MedicalAppointment> MedicalAppointments)
        {
            try
            {
                List<MedicalAppointmentModel> MedicalAppointmentModels = new List<MedicalAppointmentModel>();
                foreach (MedicalAppointment MedicalAppointment in MedicalAppointments)
                {
                    MedicalAppointmentModels.Add(TransformToModel(MedicalAppointment));
                }
                return MedicalAppointmentModels;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }
    }
}
