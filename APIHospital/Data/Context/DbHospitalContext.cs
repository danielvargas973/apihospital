﻿using APIHospital.Entity;
using Microsoft.EntityFrameworkCore;
using APIHospital.Model;

namespace APIHospital.Data.Context
{
    public class DbHospitalContext : DbContext
    {
        public DbHospitalContext(DbContextOptions<DbHospitalContext> options) : base(options)
        {

        }
        public DbSet<Doctor> Doctor { get; set; }
        public DbSet<Patient> Patient { get; set; }
        public DbSet<MedicalAppointment> MedicalAppointment { get; set; }
        public DbSet<APIHospital.Model.MedicalAppointmentModel> MedicalAppointmentModel { get; set; }
    }
}
