﻿namespace APIHospital.Entity
{
    public class Doctor
    {
        public Doctor(int id, string sT_fullName, string sT_specialty, int iN_credentialNumber, string sT_nmeHospital)
        {
            Id = id;
            ST_fullName = sT_fullName;
            ST_specialty = sT_specialty;
            IN_credentialNumber = iN_credentialNumber;
            ST_nmeHospital = sT_nmeHospital;
        }

        public int Id { get; set; }
        public string ST_fullName { get; set; }
        public string ST_specialty { get; set; }
        public int IN_credentialNumber { get; set; }
        public string ST_nmeHospital { get; set; }
    }
}
