﻿using System;

namespace APIHospital.Entity
{
    public class MedicalAppointment
    {
        public MedicalAppointment(int id, int doctorId, int patientId, int iN_officeNumber, string sT_ReasonQuote)
        {
            Id = id;
            DoctorId = doctorId;
            PatientId = patientId;
            IN_officeNumber = iN_officeNumber;
            ST_ReasonQuote = sT_ReasonQuote;
        }

        public int Id { get; set; }
        public int DoctorId { get; set; }
        public Doctor Doctor { get; set; }
        public int PatientId { get; set; }
        public Patient Patient { get; set; }
        public int IN_officeNumber { get; set; }
        public string ST_ReasonQuote { get; set; }
    }
}
