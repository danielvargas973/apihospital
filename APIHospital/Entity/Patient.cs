﻿namespace APIHospital.Entity
{
    public class Patient
    {
        public Patient(int id, string sT_fullName, int iN_socialSecurityName, int iN_postCode, int iN_telephoneContact)
        {
            Id = id;
            ST_fullName = sT_fullName;
            IN_socialSecurityName = iN_socialSecurityName;
            IN_postCode = iN_postCode;
            IN_telephoneContact = iN_telephoneContact;
        }

        public int Id { get; set; }
        public string ST_fullName { get; set; }
        public int IN_socialSecurityName { get; set; }
        public int IN_postCode { get; set; }
        public int IN_telephoneContact { get; set; }
    }
}
