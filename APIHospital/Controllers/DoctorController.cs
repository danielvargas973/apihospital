﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHospital.Data.Context;
using APIHospital.Model;
using APIHospital.Deal.Conf;
using APIHospital.Entity;
using Microsoft.AspNetCore.Cors;

namespace APIHospital.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoctorController : ControllerBase
    {
        private readonly DbHospitalContext _context;
        public DoctorController(DbHospitalContext context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult<List<DoctorModel>>> GetDoctorModel()
        {
            try
            {
                List<DoctorModel> doctorModels = new List<DoctorModel>();
                List<Doctor> doctor = await _context.Doctor.ToListAsync();
                if (doctor.Count() > 0)
                {
                    doctorModels = DoctorUtilities.TransformToModelCollection(doctor);
                }
                return doctorModels;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<DoctorModel>> GetDoctorModel(int id)
        {
            try
            {
                var doctorModel = DoctorUtilities.TransformToModel(await _context.Doctor.FindAsync(id));
                if (doctorModel == null)
                {
                    return NotFound();
                }
                return doctorModel;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDoctorModel(int id, DoctorModel doctorModel)
        {
            try
            {
                if (id != doctorModel.Id)
                {
                    return BadRequest();
                }

                _context.Entry(DoctorUtilities.TransformToEntity(doctorModel)).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DoctorModelExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return NoContent();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }
        [HttpPost]
        public async Task<ActionResult<DoctorModel>> PostDoctorModel(DoctorModel doctorModel)
        {
            try
            {
                _context.Doctor.Add(DoctorUtilities.TransformToEntity(doctorModel));
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetDoctorModel", new { id = doctorModel.Id }, doctorModel);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<DoctorModel>> DeleteDoctorModel(int id)
        {
            try
            {
                var Doctor = await _context.Doctor.FindAsync(id);
                if (Doctor == null)
                {
                    return NotFound();
                }

                _context.Doctor.Remove(Doctor);
                await _context.SaveChangesAsync();

                return DoctorUtilities.TransformToModel(Doctor);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        private bool DoctorModelExists(int id)
        {
            return _context.Doctor.Any(e => e.Id == id);
        }
    }
}
