﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHospital.Data.Context;
using APIHospital.Model;
using APIHospital.Deal.Conf;
using APIHospital.Entity;
using Microsoft.AspNetCore.Cors;

namespace APIHospital.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientController : ControllerBase
    {
        private DbHospitalContext _context;

        public PatientController(DbHospitalContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<List<PatientModel>>> GetPatientModel()
        {
            try
            {
                return PatientUtilities.TransformToModelCollection(await _context.Patient.ToListAsync());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PatientModel>> GetPatientModel(int id)
        {
            try
            {
                var patientModel = PatientUtilities.TransformToModel(await _context.Patient.FindAsync(id));

                if (patientModel == null)
                {
                    return NotFound();
                }

                return patientModel;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutPatientModel(int id, PatientModel patientModel)
        {
            try
            {
                if (id != patientModel.Id)
                {
                    return BadRequest();
                }

                _context.Entry(PatientUtilities.TransformToEntity(patientModel)).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PatientModelExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return NoContent();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        [HttpPost]
        public async Task<ActionResult<PatientModel>> PostPatientModel(PatientModel patientModel)
        {
            try
            {
                _context.Patient.Add(PatientUtilities.TransformToEntity(patientModel));
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetPatientModel", new { id = patientModel.Id }, patientModel);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<PatientModel>> DeletePatientModel(int id)
        {
            try
            {
                var patient = await _context.Patient.FindAsync(id);
                if (patient == null)
                {
                    return NotFound();
                }
                _context.Patient.Remove(patient);
                await _context.SaveChangesAsync();

                return PatientUtilities.TransformToModel(patient);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        private bool PatientModelExists(int id)
        {
            try
            {
                return _context.Patient.Any(e => e.Id == id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }
    }
}
