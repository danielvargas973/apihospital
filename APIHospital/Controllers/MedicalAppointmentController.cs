﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHospital.Data.Context;
using APIHospital.Model;
using APIHospital.Deal.Conf;
using Microsoft.AspNetCore.Cors;

namespace APIHospital.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MedicalAppointmentController : ControllerBase
    {
        private readonly DbHospitalContext _context;

        public MedicalAppointmentController(DbHospitalContext context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MedicalAppointmentModel>>> GetMedicalAppointmentModel()
        {
            return MedicalAppointmentUtilities.TransformToModelCollection(await _context.MedicalAppointment.ToListAsync());
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<MedicalAppointmentModel>> GetMedicalAppointmentModel(int id)
        {
            var medicalAppointmentModel = await _context.MedicalAppointment.FindAsync(id);

            if (medicalAppointmentModel == null)
            {
                return NotFound();
            }

            return MedicalAppointmentUtilities.TransformToModel(medicalAppointmentModel);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedicalAppointmentModel(int id, MedicalAppointmentModel medicalAppointmentModel)
        {
            if (id != medicalAppointmentModel.Id)
            {
                return BadRequest();
            }

            _context.Entry(MedicalAppointmentUtilities.TransformToEntity(medicalAppointmentModel)).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicalAppointmentModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<MedicalAppointmentModel>> PostMedicalAppointmentModel(MedicalAppointmentModel medicalAppointmentModel)
        {
            _context.MedicalAppointment.Add(MedicalAppointmentUtilities.TransformToEntity(medicalAppointmentModel));
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMedicalAppointmentModel", new { id = medicalAppointmentModel.Id }, medicalAppointmentModel);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<MedicalAppointmentModel>> DeleteMedicalAppointmentModel(int id)
        {
            var medicalAppointmentModel = await _context.MedicalAppointment.FindAsync(id);
            if (medicalAppointmentModel == null)
            {
                return NotFound();
            }

            _context.MedicalAppointment.Remove(medicalAppointmentModel);
            await _context.SaveChangesAsync();

            return MedicalAppointmentUtilities.TransformToModel(medicalAppointmentModel);
        }

        private bool MedicalAppointmentModelExists(int id)
        {
            return _context.MedicalAppointment.Any(e => e.Id == id);
        }
    }
}
